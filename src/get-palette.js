const imgToPalette = require('img-to-palette');

module.exports = fileName => {
  const canvas = document.getElementById('canvas');
  const context = canvas.getContext('2d');

  // draw loading pixelated screen
  const colors = ['#ffd4f5', '#ffd4db', '#ffddd4', '#ffe5d4', '#ffecd4'];
  const size = 20;
  for (let i = 0; i < canvas.width; i += size) {
    for (let j = 0; j < canvas.height; j += size) {
      const color = colors[`${Math.floor(Math.random() * colors.length)}`];
      context.fillStyle = color;
      context.fillRect(i, j, size, size);
    }
  }

  // load image
  const image = new Image();
  image.src = fileName;

  image.onerror = e => {
    console.error('Error loading image', e);
  };

  image.onload = e => {
    const margin = image.width * 0.01;
    const tileWidth = (image.width - margin) / 8;
    const tileHeight = image.height * 0.15;
    canvas.width = image.width;
    canvas.height = image.height + tileHeight + 2 * margin;

    // draw image and background
    context.clearRect(0, 0, canvas.width, canvas.height);
    context.fillStyle = 'white';
    context.fillRect(0, 0, canvas.width, canvas.height);
    context.drawImage(image, 0, 0, image.width, image.height);

    // draw palette
    const palette = imgToPalette(image);
    palette.forEach((v, i) => {
      context.fillStyle = `rgb(${v[0]},${v[1]},${v[2]})`;
      context.fillRect(
        i * tileWidth + margin,
        image.height + margin,
        tileWidth - margin,
        tileHeight
      );
    });

    // returning an img instead of canvas for responsiveness
    let data = canvas.toDataURL();
    context.clearRect(0, 0, canvas.width, canvas.height);
    const img = new Image();
    img.src = data;

    canvas.width = 0;
    canvas.height = 0;
    document.getElementById('can').appendChild(img);
  };
};
