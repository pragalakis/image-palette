const html = require('choo/html');
const choo = require('choo');
const css = require('sheetify');
const getPalette = require('./get-palette.js');
const dataURItoBlob = require('./datauri-to-blob.js');

const app = choo();
app.use(statusStore);
app.use(hoverStore);
app.route('/', mainView);
app.route('/image-palette', mainView);
app.mount('body');

function mainView(state, emit) {
  emit('DOMTitleChange', 'Image to Color Palette Generator');
  return html`
    <body class=${prefix}>
      <div id="flex" class=${state.hover ? 'hover' : ''}>
        <div style=${state.status ? 'display: none' : 'display: block'}>
          <div
            id="drop_zone"
            onclick=${clickHandler}
            ondrop=${dropHandler}
            ondragenter=${dragEnterHandler}
            ondragover=${dragOverHandler}
            ondragleave=${dragLeaveHandler}
          >
            <p>d r o p</p>
            <p>y o u r</p>
            <p>i m a g e</p>
            <p>h e r e</p>
          </div>
        </div>
        <div id='can' style=${
          state.status ? 'display: block' : 'display: none'
        }>
          <div>
            <button onclick=${changeStatus}>
              Drop another!
            </button>
            <button onclick=${downloadImage}>
              Download
            </button>
          </div>
          <canvas id="canvas" width="420" height="340"></canvas>
        </div>
      </div>
      <div id="footer">
        <h5>
          Made with <a href="https://choo.io/">choo</a> & coffee.
          Image Palette is <a href="https://www.gitlab.com/pragalakis/image-palette">open-source</a>
        </h5> 
      </div>
    </body>
  `;

  function changeStatus() {
    emit('changeStatus');
  }

  function changeHoverColor() {
    emit('changeHoverColor');
  }

  function downloadImage() {
    // get dataURI from img
    let data = document.getElementsByTagName('img')[0].src;
    // convert it to blob to avoid chrome's long dataURI download bug
    let blob = dataURItoBlob(data);

    let link = document.createElement('a');
    link.download = 'img-palette.png';
    link.href = URL.createObjectURL(blob);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  function handleImage(file) {
    const imageType = /^image\//;
    // exits if a non-image is dropped
    if (!imageType.test(file.type)) {
      console.error('Not supported file', file);
      return;
    }

    const reader = new FileReader();

    reader.onloadstart = event => {
      changeStatus();
    };

    reader.onload = event => {
      const imgSrc = event.target.result;
      getPalette(imgSrc);
    };

    reader.onerror = event => {
      console.error(event);
      reader.abort();
    };

    reader.readAsDataURL(file);
  }

  function clickHandler(event) {
    let form = document.createElement('form');
    let input = document.createElement('input');
    let submit = document.createElement('input');
    input.type = 'file';
    input.id = 'input-img';
    submit.type = 'button';
    submit.id = 'submit-img';
    form.id = 'form-img';
    form.style.display = 'none';
    document.body.appendChild(form);
    document.getElementById('form-img').appendChild(input);
    document.getElementById('form-img').appendChild(submit);
    input.click();

    document.getElementById('input-img').onchange = function() {
      document.getElementById('submit-img').click();
      let file = document.getElementById('input-img').files[0];
      handleImage(file);

      document.body.removeChild(form);
    };
  }

  function dropHandler(event) {
    event.preventDefault();
    changeHoverColor();
    const file = event.dataTransfer.items[0].getAsFile();
    handleImage(file);
  }

  function dragOverHandler(event) {
    event.stopPropagation();
    event.preventDefault();
  }

  function dragEnterHandler(event) {
    changeHoverColor();
  }

  function dragLeaveHandler(event) {
    changeHoverColor();
  }
}

function statusStore(state, emitter) {
  emitter.on('changeStatus', status => {
    state.status = state.status ? false : true;
    emitter.emit('render');
  });
}

function hoverStore(state, emitter) {
  emitter.on('changeHoverColor', hover => {
    state.hover = state.hover ? false : true;
    emitter.emit('render');
  });
}

const prefix = css`
  html {
    height: 100%;
  }

  :host {
    height: 100%;
    margin: 0;
    text-align: center;
    background-color: #ffe5e5;
    font-family: Consolas, monaco, monospace;
  }

  :host #flex {
    height: 92%;
    display: flex;
    flex-direction: column;
    justify-content: center;
  }

  :host #drop_zone {
    width: 410px;
    height: 330px;
    margin: 0 auto;
    border: 5px dashed #f77;
    z-index: 10;
  }

  :host .hover #drop_zone {
    border-color: #000;
  }

  :host p {
    margin-top: 2.5rem;
    color: #f77;
    font-weight: bold;
    font-size: 1.5rem;
    z-index: 1;
  }

  :host .hover p {
    color: #000;
  }

  :host button {
    font-family: Consolas, monaco, monospace;
    background-color: #f77;
    padding: 10px 15px;
    margin: 15px 4px;
    border: none;
    font-weight: bold;
    font-size: 15px;
    color: #ffe5e5;
    letter-spacing: 2px;
    outline: none;
    cursor: pointer;
  }

  img {
    width: auto;
    max-height: 60vh;
  }

  :host #footer {
    text-align: center;
  }

  @media screen and (max-width: 420px) {
    :host #drop_zone {
      width: 300px;
      height: 310px;
    }

    img {
      max-width: 80vw;
      max-height: 60vh;
    }
  }
`;
