# image-palette

Color palette generator website

#### [https://pragalakis.gitlab.io/image-palette/](https://pragalakis.gitlab.io/image-palette/)

![](screenshot.png)

### License

GNU AGPLv3, see [LICENSE](LICENSE) for details.
